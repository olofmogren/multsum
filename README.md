# An implementation of Submodular Optimization for Extractive Multi-Document Summarization

Olof Mogren

(Or rather, two implementations, there is one in Java, and one in Python).

This repo has moved to https://github.com/olofmogren/multsum

For more info, see http://mogren.one/summarization

